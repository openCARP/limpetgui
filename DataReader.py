# -*- coding: utf-8 -*-
# Bernardo M. Rocha, 2014
# Gilmar F. S. Filho

import h5py
import csv
import numpy as np

from util import time_names, pot_names


class DataReader(object):

    def __init__(self):
        pass

    @staticmethod
    def factory(file_extension):
        if file_extension == '.h5': return HDF5Reader()
        if file_extension == '.csv': return CSVReader()
        if file_extension == '.txt': return TXTReader()
        if file_extension == '.xls': return XLSReader()


class HDF5Reader(DataReader):

    def read(self, file, mf=None, data_set_ID=None):
        h5file = h5py.File(file, 'r')

        # convert to dictionary
        fd = {}
        ff = {data_set_ID: fd}
#       if mf is not None:
#           for key in h5file[data_set_ID].keys():
#               if (key in mf.keys()) or (key in mf.values()) or (key in time_names) or (key in pot_names):
#                   fd[key] = np.array(h5file[data_set_ID][key])
#       else:
        for key in h5file[data_set_ID].keys():
            fd[key] = np.array(h5file[data_set_ID][key]).squeeze()

        return ff

    def read_group(self, h5file, group_to_read):
        """
        reads a given group of a hdf5 file
        """
        # converts to dictionary
        fd = {}
        ff = {group_to_read: fd}

        for key in h5file[group_to_read].keys():
            fd[key] = np.array(h5file[group_to_read][key])

        return ff

    def obtain_groups(self, file):
        """
        detects all valid groups in the hdf5 file and returns a list of those groups and the h5file
        """
        h5file = h5py.File(file, 'r')

        substr = 'protocol_'
        group_list = []
        for key in h5file.keys():
            if substr in key:
                group_list.append(key)

        return [h5file, group_list]


class CSVReader(DataReader):

    def read(self, file, mf=None):
        csvfile = open(file, 'rb')
        reader = csv.reader(csvfile)

        fd = {}
        ff = {'protocol_1': fd}

        rownum = 0
        for row in reader:
            if rownum == 0:
                svnames = row
            else:
                colnum = 0
                if mf is not None:
                    for col in row:
                        if (svnames[colnum] in mf.keys()) or (svnames[colnum] in mf.values()) \
                                or (svnames[colnum] in time_names) or (svnames[colnum] in pot_names):
                            key = svnames[colnum]
                            if rownum == 1: fd[key] = []
                            fd[key].append(float(col))
                        colnum += 1
                else:
                    for col in row:
                        key = svnames[colnum]
                        if rownum == 1: fd[key] = []
                        fd[key].append(float(col))
                        colnum += 1
            rownum += 1

        csvfile.close()

        return ff


class TXTReader(DataReader):

    def read(self, file, mf):
        txtfile = open(file, mode='r')

        fd = {}
        ff = {'protocol_1': fd}

        rownum = 0
        for line in txtfile:
            if rownum == 0:
                svnames = line[:len(line)-1].split(',')
            else:
                colnum = 0
                row = line[:len(line)-1].split(',')
                if mf is not None:
                    for col in row:
                        if (svnames[colnum] in mf.keys()) or (svnames[colnum] in mf.values()) \
                                or (svnames[colnum] in time_names) or (svnames[colnum] in pot_names):
                            key = svnames[colnum]
                            if rownum == 1: fd[key] = []
                            fd[key].append(float(col))
                        colnum += 1
                else:
                    for col in row:
                        key = svnames[colnum]
                        if rownum == 1: fd[key] = []
                        fd[key].append(float(col))
                        colnum += 1
            rownum += 1

        txtfile.close()

        return ff


class XLSReader(DataReader):

    def read(self, file, mf):
        pass
