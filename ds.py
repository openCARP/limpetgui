## -----------------------------------------------------------------------------
## Code for downsampling data
## -----------------------------------------------------------------------------

#from memory_profiler import profile
import numpy as np

#@profile
def downsample(data):
    
    start = 0
    stop = len(data)
    
    limit = 10000
    limit = (stop-start)/2
    
    print("ORIGINAL SIZE=%d" % stop)

    ds = int((stop-start) / limit) + 1

    if ds == 1:
        print("small enough")
        #scale = 1
    else:

        samples = 1 + ((stop-start) // ds)
        visible = np.zeros(samples*2, dtype=np.float32)
        sourcePtr = start
        targetPtr = 0

        # read data in chunks of ~1M samples
        chunkSize = (1000000//ds) * ds
        while sourcePtr < stop-1:
            chunk = data[sourcePtr:min(stop,sourcePtr+chunkSize)]
            sourcePtr += len(chunk)

            # reshape chunk to be integral multiple of ds
            chunk = chunk[:(len(chunk)//ds) * ds].reshape(len(chunk)//ds, ds)
                
            # compute max and min
            chunkMax = chunk.max(axis=1)
            chunkMin = chunk.min(axis=1)

            # interleave min and max into plot data to preserve envelope shape
            visible[targetPtr:targetPtr+chunk.shape[0]*2:2] = chunkMin
            visible[1+targetPtr:1+targetPtr+chunk.shape[0]*2:2] = chunkMax
            targetPtr += chunk.shape[0]*2 

        visible = visible[:targetPtr]
        #scale = ds * 0.5
    print("MODIFIED SIZE=%d" % visible.size)
    return visible
    
#def downsampleHDF5Dataset(data):
#    
#    limit = 10000
#
#    start = 0
#    stop = len(data)
#
#    ds = int((stop-start) / limit) + 1
#
#    if ds == 1:
#        print("small enough")
#        #scale = 1
#    else:
#
#        samples = 1 + ((stop-start) // ds)
#        visible = np.zeros(samples*2, dtype=np.float32)
#        sourcePtr = start
#        targetPtr = 0
#
#        # read data in chunks of ~1M samples
#        chunkSize = (1000000//ds) * ds
#        while sourcePtr < stop-1:
#            chunk = data[sourcePtr:min(stop,sourcePtr+chunkSize)]
#            sourcePtr += len(chunk)
#
#            # reshape chunk to be integral multiple of ds
#            chunk = chunk[:(len(chunk)//ds) * ds].reshape(len(chunk)//ds, ds)
#                
#            # compute max and min
#            chunkMax = chunk.max(axis=1)
#            chunkMin = chunk.min(axis=1)
#
#            # interleave min and max into plot data to preserve envelope shape
#            visible[targetPtr:targetPtr+chunk.shape[0]*2:2] = chunkMin
#            visible[1+targetPtr:1+targetPtr+chunk.shape[0]*2:2] = chunkMax
#            targetPtr += chunk.shape[0]*2 
#
#        visible = visible[:targetPtr]
#        #scale = ds * 0.5
#
#    return visible    
