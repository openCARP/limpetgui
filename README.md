
# LimpetGUI 0.1

The LimpetGUI package contains the executables `limpetgui`, `bin2h5` and `txt2h5`.

## Installation instructions

### Install with pip

LimpetGUI can be installed using `pip`. It is recommended to install LimpetGUI within a virtual environment.

On Unix systems, you can for example use:
```
python -m venv venv_limpetgui
. ./venv_limpetgui/bin/activate
pip install git+https://git.opencarp.org/openCARP/limpetgui.git
```

You can then run the main program after ensuring the virtual environment is activated:
```
. ./venv_limpetgui/bin/activate
limpetgui
```

### Install with pipx

Alternatively, you can install LimpetGUI using [pipx](https://github.com/pypa/pipx), with:
```
pipx install git+https://git.opencarp.org/openCARP/limpetgui.git
```

## CHANGELOG

* New version of LimpetGUI using PyQtGraph
