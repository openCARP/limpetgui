# -*- coding: utf-8 -*-
# Bernardo M. Rocha, 2014
# Gilmar F. S. Filho

import os
from PyQt5.QtWidgets import *
import numpy as np
import pyqtgraph as pg
from math import ceil
from pyqtgraph.Qt import QtGui, QtCore

from PyQt5.QtGui import QPixmap
import PyQt5.QtWidgets as QtWidgets

import util
from util import search_var
from DataReader import DataReader

# ------------------------------------------------------------------------------
# Some PyQtGraph definitions

mypen1 = pg.mkPen(width=1, style=QtCore.Qt.SolidLine, color='r')
mypen2 = pg.mkPen(width=1, style=QtCore.Qt.SolidLine, color='b')
pg.setConfigOption('background', 'w')  # white bg and black fg
# pg.setConfigOption('foreground', 'k') # other way around
pg.setConfigOptions(antialias=True)  # antialiasing for prettier plots

# QtGui.QApplication.setGraphicsSystem('raster')


# ------------------------------------------------------------------------------


class SVPlot:

    def __init__(self, t, sv1, sv2, p, lr, t2=None):
        self.variable = sv1
        self.variable2 = sv2
        self.plot = p
        self.plotList = lr
        self.timeAxis = t

        p.plot(x=t, y=sv1, pen=mypen1)

        if sv2 is not None:
            if t2 is None:
                p.plot(x=t, y=sv2, pen=mypen2)
            else:
                p.plot(x=t2, y=sv2, pen=mypen2)

        # Downsample
        p.setDownsampling(auto=True, mode='peak')
        p.getViewBox().setMouseMode(pg.ViewBox.RectMode)

        # create the "plot line" sub-menu for the right-button context menu
        self.zoomMenu = QtWidgets.QMenu("Zoom")

        # "zoom in" and "zoom out" buttons
        self.zoomOutAct = QtWidgets.QAction("Zoom Out   (Ctrl -)", self.zoomMenu)
        self.zoomInAct = QtWidgets.QAction("Zoom In       (Ctrl +)", self.zoomMenu)
        self.zoomMenu.addAction(self.zoomOutAct)
        self.zoomMenu.addAction(self.zoomInAct)
        self.zoomOutAct.triggered.connect(self.zoomOut)
        self.zoomInAct.triggered.connect(self.zoomIn)

        # add the "plot line" and "zoom" sub-menus to the right-button context menu
        p.ctrlMenu = [self.zoomMenu, p.ctrlMenu]

        # this is to fix the 2-click zoom bug
        xmin, xmax = p.getViewBox().viewRange()[0]
        p.setXRange(xmin, xmax, padding=0)

        p.showGrid(x=False, y=False)

    def zoomOut(self):
        """
        Moves backward in the zooming stack (if it exists)
        """
        if self.plot.getViewBox().axHistoryPointer != 0:
            self.plot.getViewBox().scaleHistory(-1)
        else:
            self.plot.autoRange()

    def zoomIn(self):
        """
        Moves forward in the zooming stack (if it exists)
        """
        self.plot.getViewBox().scaleHistory(1)

    def connectSignal(self, single=True):
        if single:
            self.plot.sigXRangeChanged.connect(self.updateRegionSingle)
        else:
            self.plot.sigXRangeChanged.connect(self.updateRegionUniversal)

    def updateRegionSingle(self):
        """
        Enable single zoom
        """
        p = self.plot
        xmin, xmax = p.getViewBox().viewRange()[0]
        p.setXRange(xmin, xmax, padding=0)

    def updateRegionUniversal(self):
        """
        Enable universal zoom
        """
        p = self.plot
        xmin, xmax = p.getViewBox().viewRange()[0]
        for mp in self.plotList:
            mp.plot.setXRange(xmin, xmax, padding=0)


# ------------------------------------------------------------------------------


class MyDialog(QtWidgets.QDialog):

    def __init__(self, report, title, parent=None):
        super(MyDialog, self).__init__(parent)

        self.initReport(report, title)

    def initReport(self, report, title):
        self.textBrowser = report

        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.verticalLayout.addWidget(self.textBrowser)
        self.setWindowTitle(title)
        self.setFixedWidth(450)
        self.setFixedHeight(600)


# ------------------------------------------------------------------------------


class GroupSelection(QtWidgets.QDialog):

    def __init__(self, grouplst):

        super(GroupSelection, self).__init__()

        self.group_list = grouplst
        self.canceled = None

        self.initUI()

    def initUI(self):

        # labels and buttons
        self.label = QtWidgets.QLabel(self)
        self.label.setText('Select one or two groups to open:')
        self.label.adjustSize()

        cbtn = QtWidgets.QPushButton('Cancel', self)
        cbtn.clicked.connect(self.quit)
        cbtn.resize(cbtn.sizeHint())

        okbtn = QtWidgets.QPushButton('Ok', self)
        okbtn.clicked.connect(self.select_group)
        okbtn.resize(cbtn.sizeHint())

        # checklist with the groups to be opened
        self.chklst = []
        lst = QtWidgets.QListWidget()

        for i in range(len(self.group_list)):
            new_string = '%s' % (self.group_list[i])
            new_item = QtWidgets.QListWidgetItem(new_string)
            new_item.setCheckState(QtCore.Qt.Unchecked)
            new_item.setTextAlignment(0x0001)
            self.chklst.append(QtCore.Qt.Unchecked)
            lst.insertItem(i, new_item)

        self.list_widget = lst
        self.list_widget.itemChanged[QListWidgetItem].connect(self.update_items)

        # configure layout
        grid = QtWidgets.QGridLayout()
        grid.setSpacing(10)

        grid.addWidget(self.label, 0, 0)
        grid.addWidget(self.list_widget, 1, 0, QtCore.Qt.AlignJustify)
        grid.addWidget(cbtn, 2, 0, QtCore.Qt.AlignLeft)
        grid.addWidget(okbtn, 2, 0, QtCore.Qt.AlignRight)

        self.setLayout(grid)

        self.setFixedWidth(250)
        self.setFixedHeight(350)
        self.setWindowTitle('Group Selection')
        self.show()

    def quit(self):

        self.canceled = True
        self.close()

    def select_group(self):

        self.group_to_open = []
        for i in range(len(self.group_list)):
            if self.chklst[i] == QtCore.Qt.Checked:
                self.group_to_open.append(self.group_list[i])

        grpqtt = len(self.group_to_open)
        if grpqtt > 2 or grpqtt == 0:
            QMessageBox.about(self, "Error", """Select up to two groups!\n""")
            self.group_to_open = []

        self.canceled = False
        self.close()

    def update_items(self, myint):
        """
        Update the properties (on/off) of the groups whenever the
        group is checked or unchecked in the list_widget
        """

        self.update = True

        state = myint.checkState()
        mtmp = myint.text().split("/t")
        grp = str(mtmp[0])

        for i in range(len(self.group_list)):
            if self.group_list[i] == grp:
                if self.chklst[i] == QtCore.Qt.Unchecked:
                    self.chklst[i] = QtCore.Qt.Checked
                else:
                    self.chklst[i] = QtCore.Qt.Unchecked
                break

    def get_group_to_open(self):

        return self.group_to_open


# ------------------------------------------------------------------------------

# TODO: menu button that shows the groups of an opened file

class MainWindow(QMainWindow):

    def __init__(self, stateVarList, data, groups, parent=None):
        super(MainWindow, self).__init__(parent)

        # create the central widget
        self.central_widget = LimpetGUI(stateVarList, data, groups)

        # create the menu
        self.menu()

        self.setCentralWidget(self.central_widget)
        self.showMaximized()
        self.setWindowTitle('LimpetGUI')

    def menu(self):
        """
        Create the menu
        """
        self.menubar = self.menuBar()

        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")

        self.subMenuOpen = QtWidgets.QMenu(self.menubar)
        self.subMenuOpen.setObjectName("subMenuOpen")

        self.subMenuSaveAs = QtWidgets.QMenu(self.menubar)
        self.subMenuSaveAs.setObjectName("subMenuSaveAs")

        self.subMenuPreferences = QtWidgets.QMenu(self.menubar)
        self.subMenuPreferences.setObjectName("subMenuPreferences")

        self.subMenuFile1 = QtWidgets.QMenu(self.menubar)
        self.subMenuFile1.setObjectName("subMenuFile1")

        self.subMenuFile2 = QtWidgets.QMenu(self.menubar)
        self.subMenuFile2.setObjectName("subMenuFile2")

        self.subMenuLineColorFile1 = QtWidgets.QMenu(self.menubar)
        self.subMenuLineColorFile1.setObjectName("subMenuLineColor")

        self.subMenuLineWidthFile1 = QtWidgets.QMenu(self.menubar)
        self.subMenuLineWidthFile1.setObjectName("subMenuLineWidth")

        self.subMenuLineColorFile2 = QtWidgets.QMenu(self.menubar)
        self.subMenuLineColorFile2.setObjectName("subMenuLineColor")

        self.subMenuLineWidthFile2 = QtWidgets.QMenu(self.menubar)
        self.subMenuLineWidthFile2.setObjectName("subMenuLineWidth")

        self.menuTools = QtWidgets.QMenu(self.menubar)
        self.menuTools.setObjectName("menuTools")

        self.subMenuCalculateAPD = QtWidgets.QMenu(self.menubar)
        self.subMenuCalculateAPD.setObjectName("subMenuCaculateAPD")

        self.subMenuPlotAPDRest = QtWidgets.QMenu(self.menubar)
        self.subMenuPlotAPDRest.setObjectName("subMenuPlotAPDRest")

        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")

        self.lineColorActionGroup1 = QtWidgets.QActionGroup(self.menubar)

        self.actionRed1 = QtWidgets.QAction(self)
        self.actionRed1.setObjectName("actionRed1")

        self.actionGreen1 = QtWidgets.QAction(self)
        self.actionGreen1.setObjectName("actionGreen1")

        self.actionBlue1 = QtWidgets.QAction(self)
        self.actionBlue1.setObjectName("actionBlue1")

        self.actionYellow1 = QtWidgets.QAction(self)
        self.actionYellow1.setObjectName("actionYellow1")

        self.actionBlack1 = QtWidgets.QAction(self)
        self.actionBlack1.setObjectName("actionBlack1")

        self.lineColorActionGroup2 = QtWidgets.QActionGroup(self.menubar)

        self.actionRed2 = QtWidgets.QAction(self)
        self.actionRed2.setObjectName("actionRed2")

        self.actionGreen2 = QtWidgets.QAction(self)
        self.actionGreen2.setObjectName("actionGreen2")

        self.actionBlue2 = QtWidgets.QAction(self)
        self.actionBlue2.setObjectName("actionBlue2")

        self.actionYellow2 = QtWidgets.QAction(self)
        self.actionYellow2.setObjectName("actionYellow2")

        self.actionBlack2 = QtWidgets.QAction(self)
        self.actionBlack2.setObjectName("actionBlack2")

        self.lineWidthActionGroup1 = QtWidgets.QActionGroup(self.menubar)

        self.actionWidth11 = QtWidgets.QAction(self)
        self.actionWidth11.setObjectName("actionWidth11")

        self.actionWidth21 = QtWidgets.QAction(self)
        self.actionWidth21.setObjectName("actionWidth21")

        self.actionWidth31 = QtWidgets.QAction(self)
        self.actionWidth31.setObjectName("actionWidth31")

        self.lineWidthActionGroup2 = QtWidgets.QActionGroup(self.menubar)

        self.actionWidth12 = QtWidgets.QAction(self)
        self.actionWidth12.setObjectName("actionWidth12")

        self.actionWidth22 = QtWidgets.QAction(self)
        self.actionWidth22.setObjectName("actionWidth22")

        self.actionWidth32 = QtWidgets.QAction(self)
        self.actionWidth32.setObjectName("actionWidth32")

        self.actionOpenFile1 = QtWidgets.QAction(self)
        self.actionOpenFile1.setObjectName("actionOpenFile1")

        self.actionOpenFile2 = QtWidgets.QAction(self)
        self.actionOpenFile2.setObjectName("actionOpenFile2")

        # ------------------------------------------------------------
        # self.actionSaveState = QtGui.QAction(self)
        # self.actionSaveState.setObjectName("actionSaveState")
        #
        # self.actionLoadState = QtGui.QAction(self)
        # self.actionLoadState.setObjectName("actionLoadState")
        # ------------------------------------------------------------

        self.actionPNG = QtWidgets.QAction(self)
        self.actionPNG.setObjectName("actionPNG")

        self.actionExit = QtWidgets.QAction(self)
        self.actionExit.setObjectName("actionExit")

        self.actionComputeError = QtWidgets.QAction(self)
        self.actionComputeError.setObjectName("actionComputeError")

        self.actionAPDFile1 = QtWidgets.QAction(self)
        self.actionAPDFile1.setObjectName("actionAPDFile1")

        self.actionAPDFile2 = QtWidgets.QAction(self)
        self.actionAPDFile2.setObjectName("actionAPDFile2")

        self.actionAPDRestF1 = QtWidgets.QAction(self)
        self.actionAPDRestF1.setObjectName("actionAPDRestF1")

        self.actionAPDRestF2 = QtWidgets.QAction(self)
        self.actionAPDRestF2.setObjectName("actionAPDRestF2")

        self.actionAbout_Limpet = QtWidgets.QAction(self)
        self.actionAbout_Limpet.setObjectName("actionAbout_Limpet")

        self.subMenuOpen.addAction(self.actionOpenFile1)
        self.subMenuOpen.addAction(self.actionOpenFile2)
        self.subMenuSaveAs.addAction(self.actionPNG)
        self.subMenuPreferences.addMenu(self.subMenuFile1)
        self.subMenuPreferences.addMenu(self.subMenuFile2)
        self.subMenuFile1.addMenu(self.subMenuLineColorFile1)
        self.subMenuFile1.addMenu(self.subMenuLineWidthFile1)
        self.subMenuLineColorFile1.addAction(self.actionRed1)
        self.subMenuLineColorFile1.addAction(self.actionGreen1)
        self.subMenuLineColorFile1.addAction(self.actionBlue1)
        self.subMenuLineColorFile1.addAction(self.actionYellow1)
        self.subMenuLineColorFile1.addAction(self.actionBlack1)
        self.subMenuLineWidthFile1.addAction(self.actionWidth11)
        self.subMenuLineWidthFile1.addAction(self.actionWidth21)
        self.subMenuLineWidthFile1.addAction(self.actionWidth31)
        self.subMenuFile2.addMenu(self.subMenuLineColorFile2)
        self.subMenuFile2.addMenu(self.subMenuLineWidthFile2)
        self.subMenuLineColorFile2.addAction(self.actionRed2)
        self.subMenuLineColorFile2.addAction(self.actionGreen2)
        self.subMenuLineColorFile2.addAction(self.actionBlue2)
        self.subMenuLineColorFile2.addAction(self.actionYellow2)
        self.subMenuLineColorFile2.addAction(self.actionBlack2)
        self.subMenuLineWidthFile2.addAction(self.actionWidth12)
        self.subMenuLineWidthFile2.addAction(self.actionWidth22)
        self.subMenuLineWidthFile2.addAction(self.actionWidth32)
        self.subMenuCalculateAPD.addAction(self.actionAPDFile1)
        self.subMenuCalculateAPD.addAction(self.actionAPDFile2)
        self.subMenuPlotAPDRest.addAction(self.actionAPDRestF1)
        self.subMenuPlotAPDRest.addAction(self.actionAPDRestF2)
        self.menuFile.addMenu(self.subMenuOpen)
        self.menuFile.addMenu(self.subMenuSaveAs)
        # ------------------------------------------------------------
        # self.menuFile.addAction(self.actionSaveState)
        # self.menuFile.addAction(self.actionLoadState)
        # ------------------------------------------------------------
        self.menuFile.addMenu(self.subMenuPreferences)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExit)
        self.menuTools.addAction(self.actionComputeError)
        self.menuTools.addMenu(self.subMenuCalculateAPD)
        self.menuTools.addMenu(self.subMenuPlotAPDRest)
        self.menuHelp.addAction(self.actionAbout_Limpet)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuTools.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.actionRed1.setCheckable(True)
        self.actionGreen1.setCheckable(True)
        self.actionBlue1.setCheckable(True)
        self.actionYellow1.setCheckable(True)
        self.actionBlack1.setCheckable(True)
        self.actionRed1.setActionGroup(self.lineColorActionGroup1)
        self.actionGreen1.setActionGroup(self.lineColorActionGroup1)
        self.actionBlue1.setActionGroup(self.lineColorActionGroup1)
        self.actionYellow1.setActionGroup(self.lineColorActionGroup1)
        self.actionBlack1.setActionGroup(self.lineColorActionGroup1)
        self.actionRed2.setCheckable(True)
        self.actionGreen2.setCheckable(True)
        self.actionBlue2.setCheckable(True)
        self.actionYellow2.setCheckable(True)
        self.actionBlack2.setCheckable(True)
        self.actionRed2.setActionGroup(self.lineColorActionGroup2)
        self.actionGreen2.setActionGroup(self.lineColorActionGroup2)
        self.actionBlue2.setActionGroup(self.lineColorActionGroup2)
        self.actionYellow2.setActionGroup(self.lineColorActionGroup2)
        self.actionBlack2.setActionGroup(self.lineColorActionGroup2)

        self.actionWidth11.setCheckable(True)
        self.actionWidth21.setCheckable(True)
        self.actionWidth31.setCheckable(True)
        self.actionWidth11.setActionGroup(self.lineWidthActionGroup1)
        self.actionWidth21.setActionGroup(self.lineWidthActionGroup1)
        self.actionWidth31.setActionGroup(self.lineWidthActionGroup1)
        self.actionWidth12.setCheckable(True)
        self.actionWidth22.setCheckable(True)
        self.actionWidth32.setCheckable(True)
        self.actionWidth12.setActionGroup(self.lineWidthActionGroup2)
        self.actionWidth22.setActionGroup(self.lineWidthActionGroup2)
        self.actionWidth32.setActionGroup(self.lineWidthActionGroup2)

        self.menuFile.setTitle(QtCore.QCoreApplication.translate("MainWindow", "File", None))
        self.subMenuSaveAs.setTitle(QtCore.QCoreApplication.translate("MainWindow", "Save as...", None))
        self.subMenuOpen.setTitle(QtCore.QCoreApplication.translate("MainWindow", "Open", None))
        self.subMenuPreferences.setTitle(QtCore.QCoreApplication.translate("MainWindow", "Preferences", None))
        self.subMenuFile1.setTitle(QtCore.QCoreApplication.translate("MainWindow", "File 1", None))
        self.subMenuFile2.setTitle(QtCore.QCoreApplication.translate("MainWindow", "File 2", None))
        self.subMenuLineColorFile1.setTitle(QtCore.QCoreApplication.translate("MainWindow", "Plot Line Color", None))
        self.subMenuLineWidthFile1.setTitle(QtCore.QCoreApplication.translate("MainWindow", "Plot Line Width", None))
        self.subMenuLineColorFile2.setTitle(QtCore.QCoreApplication.translate("MainWindow", "Plot Line Color", None))
        self.subMenuLineWidthFile2.setTitle(QtCore.QCoreApplication.translate("MainWindow", "Plot Line Width", None))
        self.subMenuCalculateAPD.setTitle(QtCore.QCoreApplication.translate("MainWindow", "Compute Statistics", None))
        self.subMenuPlotAPDRest.setTitle(QtCore.QCoreApplication.translate("MainWindow", "Plot APD Restitution", None))
        self.menuTools.setTitle(QtCore.QCoreApplication.translate("MainWindow", "Tools", None))
        self.menuHelp.setTitle(QtCore.QCoreApplication.translate("MainWindow", "Help", None))
        self.actionRed1.setText(QtCore.QCoreApplication.translate("MainWindow", "Red", None))
        self.actionGreen1.setText(QtCore.QCoreApplication.translate("MainWindow", "Green", None))
        self.actionBlue1.setText(QtCore.QCoreApplication.translate("MainWindow", "Blue", None))
        self.actionYellow1.setText(QtCore.QCoreApplication.translate("MainWindow", "Yellow", None))
        self.actionBlack1.setText(QtCore.QCoreApplication.translate("MainWindow", "Black", None))
        self.actionRed2.setText(QtCore.QCoreApplication.translate("MainWindow", "Red", None))
        self.actionGreen2.setText(QtCore.QCoreApplication.translate("MainWindow", "Green", None))
        self.actionBlue2.setText(QtCore.QCoreApplication.translate("MainWindow", "Blue", None))
        self.actionYellow2.setText(QtCore.QCoreApplication.translate("MainWindow", "Yellow", None))
        self.actionBlack2.setText(QtCore.QCoreApplication.translate("MainWindow", "Black", None))
        self.actionWidth11.setText(QtCore.QCoreApplication.translate("MainWindow", "1", None))
        self.actionWidth21.setText(QtCore.QCoreApplication.translate("MainWindow", "2", None))
        self.actionWidth31.setText(QtCore.QCoreApplication.translate("MainWindow", "3", None))
        self.actionWidth12.setText(QtCore.QCoreApplication.translate("MainWindow", "1", None))
        self.actionWidth22.setText(QtCore.QCoreApplication.translate("MainWindow", "2", None))
        self.actionWidth32.setText(QtCore.QCoreApplication.translate("MainWindow", "3", None))
        self.actionOpenFile1.setText(QtCore.QCoreApplication.translate("MainWindow", "Open File1", None))
        self.actionOpenFile2.setText(QtCore.QCoreApplication.translate("MainWindow", "Open File2", None))
        # ------------------------------------------------------------
        # self.actionSaveState.setText(QtGui.QApplication.translate("MainWindow", "Save State", None,
        #                                                           QtGui.QApplication.UnicodeUTF8))
        # self.actionLoadState.setText(QtGui.QApplication.translate("MainWindow", "Load State", None,
        #                                                           QtGui.QApplication.UnicodeUTF8))
        # ------------------------------------------------------------
        self.actionPNG.setText(QtCore.QCoreApplication.translate("MainWindow", "PNG", None))
        self.actionExit.setText(QtCore.QCoreApplication.translate("MainWindow", "Exit", None))
        self.actionExit.setShortcut(QtCore.QCoreApplication.translate("MainWindow", "Ctrl+X", None))
        self.actionComputeError.setText(QtCore.QCoreApplication.translate("MainWindow", "Compute Error", None))
        self.actionAPDFile1.setText(QtCore.QCoreApplication.translate("MainWindow", "File 1", None))
        self.actionAPDFile2.setText(QtCore.QCoreApplication.translate("MainWindow", "File 2", None))
        self.actionAPDRestF1.setText(QtCore.QCoreApplication.translate("MainWindow", "File 1", None))
        self.actionAPDRestF2.setText(QtCore.QCoreApplication.translate("MainWindow", "File 2", None))
        self.actionAbout_Limpet.setText(QtCore.QCoreApplication.translate("MainWindow", "About Limpet", None))

        self.actionExit.triggered.connect(self.fileQuit)
        self.actionComputeError.triggered.connect(self.computeError)
        self.actionAPDFile1.triggered.connect(lambda: self.calculateAPD(1))
        self.actionAPDFile2.triggered.connect(lambda: self.calculateAPD(2))
        self.actionAPDRestF1.triggered.connect(lambda: self.plotAPDRestitution(1))
        self.actionAPDRestF2.triggered.connect(lambda: self.plotAPDRestitution(2))
        self.actionAbout_Limpet.triggered.connect(self.aboutLimpet)
        self.actionOpenFile1.triggered.connect(self.openFile1)
        self.actionOpenFile2.triggered.connect(self.openFile2)
        # ------------------------------------------------------------
        # self.connect(self.actionSaveState, SIGNAL('triggered()'), self.saveMyState)
        # self.connect(self.actionLoadState, SIGNAL('triggered()'), self.loadMyState)
        # ------------------------------------------------------------
        self.actionPNG.triggered.connect(self.savePng)
        self.actionRed1.triggered.connect(lambda: self.setPlotLineColor('#FF0000', 1))
        self.actionGreen1.triggered.connect(lambda: self.setPlotLineColor('#008000', 1))
        self.actionBlue1.triggered.connect(lambda: self.setPlotLineColor('#0000FF', 1))
        self.actionYellow1.triggered.connect(lambda: self.setPlotLineColor('#FFFF00', 1))
        self.actionBlack1.triggered.connect(lambda: self.setPlotLineColor('#000000', 1))
        self.actionRed2.triggered.connect(lambda: self.setPlotLineColor('#FF0000', 2))
        self.actionGreen2.triggered.connect(lambda: self.setPlotLineColor('#008000', 2))
        self.actionBlue2.triggered.connect(lambda: self.setPlotLineColor('#0000FF', 2))
        self.actionYellow2.triggered.connect(lambda: self.setPlotLineColor('#FFFF00', 2))
        self.actionBlack2.triggered.connect(lambda: self.setPlotLineColor('#000000', 2))
        self.actionWidth11.triggered.connect(lambda: self.setPlotLineWidth(1, 1))
        self.actionWidth21.triggered.connect(lambda: self.setPlotLineWidth(2, 1))
        self.actionWidth31.triggered.connect(lambda: self.setPlotLineWidth(3, 1))
        self.actionWidth12.triggered.connect(lambda: self.setPlotLineWidth(1, 2))
        self.actionWidth22.triggered.connect(lambda: self.setPlotLineWidth(2, 2))
        self.actionWidth32.triggered.connect(lambda: self.setPlotLineWidth(3, 2))

        # initial conditions
        self.actionWidth11.setChecked(True)
        self.actionRed1.setChecked(True)

        if self.central_widget.data1 is None and self.central_widget.data2 is None:
            self.actionOpenFile2.setDisabled(True)

        if self.central_widget.data2 is None:
            self.subMenuFile2.setDisabled(True)
            self.actionAPDFile2.setDisabled(True)
            self.actionAPDRestF2.setDisabled(True)
        else:
            self.actionBlue2.setChecked(True)
            self.actionWidth12.setChecked(True)

    def fileQuit(self):
        """
        Quit
        """
        self.close()

    def openFile1(self):
        """
        Open a valid file
        """
        cdir = os.getcwd()
        file1 = str(QFileDialog.getOpenFileName(self, 'Open file', cdir)[0])

        if file1 != '':
            # check file extensions
            if util.check_file_extension(file1):

                # reading data
                extension = util.get_file_extension(file1)
                datafile = DataReader.factory(extension)

                auxlst = datafile.obtain_groups(file1)
                h5file = auxlst[0]
                group_list = auxlst[1]

                # show groups
                groupwdw = GroupSelection(group_list)
                groupwdw.exec_()

                if groupwdw.canceled:
                    return

                # open the groups
                group_to_read1, group_to_read2, ff1, ff2 = None, None, None, None

                group_to_open = groupwdw.get_group_to_open()
                group_to_read1 = group_to_open[0]
                ff1 = datafile.read_group(h5file, group_to_read1)
                svlist1 = util.get_svlist(ff1, group_to_read1)

                error_msg = """Error: number of state variables in one file is different from the
                                       other or the two datasets have different state variables"""

                if self.central_widget.data2 is not None and len(group_to_open) == 1:

                    ff2 = self.central_widget.data2
                    group_to_read2 = self.central_widget.group2

                    if not (util.same_svquantity(svlist1, self.central_widget.svlist)) \
                            or not (util.same_svnames(svlist1, self.central_widget.svlist)):
                        QMessageBox.about(self, "Error", error_msg.replace("\n", ""))
                        return

                # if there's a second group to open and a second file is already opened
                # the second group will replace the second file/group
                if len(group_to_open) == 2:

                    group_to_read2 = group_to_open[1]
                    ff2 = datafile.read_group(h5file, group_to_read2)
                    svlist2 = util.get_svlist(ff2, group_to_read2)

                    # update the menu
                    self.subMenuFile2.setEnabled(True)
                    self.actionAPDFile2.setEnabled(True)
                    self.actionAPDRestF2.setEnabled(True)
                    self.actionBlue2.setChecked(True)
                    self.actionWidth12.setChecked(True)

                    if not util.same_svquantity(svlist1, svlist2) or not util.same_svnames(svlist1, svlist2):
                        QMessageBox.about(self, "Error", error_msg.replace("\n", ""))
                        return

                new_svlist = svlist1
                new_data = [ff1, ff2]
                groups = [group_to_read1, group_to_read2]

                new_central_widget = LimpetGUI(new_svlist, new_data, groups)
                self.central_widget = new_central_widget
                self.setCentralWidget(self.central_widget)

                # update the menu
                self.actionOpenFile2.setEnabled(True)

            else:
                QMessageBox.about(self, "Error",
                                  """Error: file %s is not a valid file\n""" % file1)

    def openFile2(self):
        """
        Open a valid file
        """
        cdir = os.getcwd()
        file2 = str(QFileDialog.getOpenFileName(self, 'Open file', cdir)[0])

        if file2 != '':
            # check file extensions
            if util.check_file_extension(file2):

                # reading data
                extension = util.get_file_extension(file2)
                datafile = DataReader.factory(extension)

                auxlst = datafile.obtain_groups(file2)
                h5file = auxlst[0]
                group_list = auxlst[1]

                # show groups
                groupwdw = GroupSelection(group_list)
                groupwdw.exec_()

                # open the groups
                group_to_open = groupwdw.get_group_to_open()
                group_to_read2 = group_to_open[0]
                ff2 = datafile.read_group(h5file, group_to_read2)
                svlist2 = util.get_svlist(ff2, group_to_read2)

                error_msg = """Error: number of state variables in one file is different from the
                                       other or the two datasets have different state variables"""

                if len(group_to_open) == 1:

                    if not (util.same_svquantity(svlist2, self.central_widget.svlist)) \
                            or not (util.same_svnames(svlist2, self.central_widget.svlist)):
                        QMessageBox.about(self, "Error", error_msg.replace("\n", ""))
                        return

                    self.central_widget.data2 = ff2
                    self.central_widget.group2 = group_to_read2

                    self.central_widget.update_plot()

                # if there's a second group to open, than it will replace the first file/group
                if len(group_to_open) == 2:

                    group_to_read1 = group_to_open[1]
                    ff1 = datafile.read_group(h5file, group_to_read1)
                    svlist1 = util.get_svlist(ff1, group_to_read1)

                    if not util.same_svquantity(svlist1, svlist2) or not util.same_svnames(svlist1, svlist2):
                        QMessageBox.about(self, "Error", error_msg.replace("\n", ""))
                        return

                    new_svlist = svlist1
                    new_data = [ff1, ff2]
                    groups = [group_to_read1, group_to_read2]

                    new_central_widget = LimpetGUI(new_svlist, new_data, groups)
                    self.central_widget = new_central_widget
                    self.setCentralWidget(self.central_widget)

                # update the menu
                self.subMenuFile2.setEnabled(True)
                self.actionAPDFile2.setEnabled(True)
                self.actionAPDRestF2.setEnabled(True)
                self.actionBlue2.setChecked(True)
                self.actionWidth12.setChecked(True)

            else:
                QMessageBox.about(self, "Error",
                                  """Error: file %s is not a valid HDF5 .h5 file\n""" % file2)

    # ------------------------------------------------------------
    # def saveMyState(self):
    #     """
    #     Saves the state of the GUI
    #     """
    #     pass
    #
    # def loadMyState(self):
    #     """
    #     Loads the state of the GUI
    #     """
    #     pass
    # ------------------------------------------------------------

    def savePng(self):
        """
        Save a PNG image file for each plot.
        """
        cdir = os.getcwd()
        istr = "Select directory and basename to save images"
        filename = QFileDialog.getSaveFileName(self.central_widget, istr, cdir)[0]

        f = '%s.png' % filename
        print(" Saving image: %s" % f)
        r = QWidget.grab(self.central_widget.plt).save(f, 'PNG')
        # r = QPixmap.grabWidget(self.central_widget.plt).save(f, 'PNG')
        if not r:
            QMessageBox.about(self, "Error", """Error saving file %s""" % f)

    @QtCore.pyqtSlot()
    def computeError(self):
        """
        Calculates the error for each state variable
        """
        # check if both files exist
        if self.central_widget.data1 is None or self.central_widget.data2 is None:
            QMessageBox.about(self, "Error", """Two files must be loaded in order to calculate the error!""")
            return

        # check if the number of values for each state variable are the same in both files
        for i in range(len(self.central_widget.svlist)):
            svname = self.central_widget.svlist[i]
            if len(self.central_widget.data1[self.central_widget.group1][svname]) != \
                    len(self.central_widget.data2[self.central_widget.group2][svname]):
                QMessageBox.about(self, 'Error', """Different number of values for the same state variable""")
                return

        # check if time variation is the same for both files
        tname = search_var(self.central_widget.data1, 'time', self.central_widget.group1)
        time_variation1 = \
            self.central_widget.data1[self.central_widget.group1][tname][1] \
            - self.central_widget.data1[self.central_widget.group1][tname][0]
        time_variation2 = \
            self.central_widget.data2[self.central_widget.group2][tname][1] \
            - self.central_widget.data2[self.central_widget.group2][tname][0]
        if time_variation1 != time_variation2:
            QMessageBox.about(self, 'Error', """The files have different time variations""")
            return

        report = QtWidgets.QTextBrowser(self.central_widget)
        report.append("Error between the two files' state variables:\n")
        report.append("State Variable\tError")
        report.append("================================")

        # calculate error
        state_variables = list(self.central_widget.data1[self.central_widget.group1].keys())
        for i in range(len(state_variables)):
            svname = state_variables[i]
            sv1 = self.central_widget.data1[self.central_widget.group1][svname]
            sv2 = self.central_widget.data2[self.central_widget.group2][svname]
            error = np.linalg.norm(sv1 - sv2)
            report.append("%s:\t\t%e" % (svname, error))

        self.central_widget.dialogTextBrowser = MyDialog(report, 'Computed Error', self.central_widget)
        self.central_widget.dialogTextBrowser.exec_()

    def calculateAPD(self, file):
        """
        Calculates the min, max and average action potential duration
        at 90% of the repolarization phase, using  the difference
        between activation and repolarization times.
        """
        perc = 0.9

        if file == 1:
            data = self.central_widget.data1
            group = self.central_widget.group1
        else:
            data = self.central_widget.data2
            group = self.central_widget.group2

        # obtain time variation and potential values
        tname = search_var(data, 'time', group)
        pot_name = search_var(data, 'potential', group)
        t = data[group][tname]
        dt = t[1] - t[0]  # time variation
        vm = data[group][pot_name]  # potential values

        dvmax = 0.0
        rep_time = 0
        act_start = 0
        rep_id = 0

        amp = np.max(vm) - np.min(vm)  # find the AP amplitude
        vchk = amp * (1.0 - perc) + np.min(vm)

        apd_list = []

        flag_apd = 0  # 0 when calculating the activation time / 1 when calculating the repolarization time

        report = QtWidgets.QTextBrowser(self.central_widget)
        report.append("Action Potential Duration's statistcs:\n")

        k = 0
        l = 0  # tells witch pulse is being calculated
        while k < (len(vm) - 2):  # and l < 60:
            for i in range(act_start, len(vm) - 1):
                dv = vm[i + 1] - vm[i]
                if vm[i] > vchk and flag_apd == 0 and dv < 0:
                    flag_apd = 1
                if dv > dvmax and flag_apd == 0:  # find the activation time
                    dvmax = dv
                    act_time = t[i]
                if vm[i] <= vchk and flag_apd == 1:  # find the repolarization time
                    rep_time = t[i]
                    rep_id = i
                    break

            k = i

            if k != (len(vm) - 2):  # so it won't calculate anything unexpected
                apd = np.abs(rep_time - act_time)  # calculate the APD

                report.append("Pulse %d:" % (l + 1))
                report.append("Activation Time:\t\t%08.2f\tms" % act_time)
                report.append("Repolarization Time:\t\t%08.2f\tms" % rep_time)
                report.append("Action Potential Duration:\t%08.2f\tms\n" % apd)

                apd_list.append(apd)

                act_start = rep_id
                dvmax = 0.0
                flag_apd = 0

            l += 1

        report.append("-------------------------------------------------------------------------------\n")

        report.append("Max APD:\t\t\t%08.2f\tms" % np.max(apd_list))
        report.append("Min APD:\t\t\t%08.2f\tms" % np.min(apd_list))
        report.append("APD's Dispersion:\t\t%08.2f\tms" % (np.max(apd_list) - np.min(apd_list)))
        report.append("Average APD:\t\t%08.2f\tms" % np.average(apd_list))
        report.append("APD's Median:\t\t%08.2f\tms" % np.median(apd_list))
        report.append("APD's Standard Deviation:\t%08.2f\tms" % np.std(apd_list))
        report.append("APD's Variance:\t\t%08.2f\tms^2\n" % np.var(apd_list))

        self.central_widget.dialogTextBrowser = MyDialog(report, 'APD Statistics', self.central_widget)
        self.central_widget.dialogTextBrowser.exec_()

    def plotAPDRestitution(self, file):
        """
        Plots the restitution APD vs Diastolic Interval
        """
        perc = 0.9

        if file == 1:
            data = self.central_widget.data1
            group = self.central_widget.group1
        else:
            data = self.central_widget.data2
            group = self.central_widget.group2

        # obtain time variation and potential values
        tname = search_var(data, 'time', group)
        pot_name = search_var(data, 'potential', group)
        t = data[group][tname]
        dt = t[1] - t[0]  # time variation
        vm = data[group][pot_name]  # potential values

        dvmax = 0.0
        rep_time = 0
        act_start = 0
        rep_id = 0

        amp = np.max(vm) - np.min(vm)  # find the AP amplitude
        vchk = amp * (1.0 - perc) + np.min(vm)

        apd_list = []
        act_time_list = []

        flag_apd = 0  # 0 when calculating the activation time / 1 when calculating the repolarization time

        k = 0
        l = 0  # tells witch pulse is being calculated
        while k < (len(vm) - 2):  # and l < 60:
            for i in range(act_start, len(vm) - 1):
                dv = vm[i + 1] - vm[i]
                if vm[i] > vchk and flag_apd == 0 and dv < 0:
                    flag_apd = 1
                if dv > dvmax and flag_apd == 0:  # find the activation time
                    dvmax = dv
                    act_time = t[i]
                if vm[i] <= vchk and flag_apd == 1:  # find the repolarization time
                    rep_time = t[i]
                    rep_id = i
                    break

            k = i

            if k != (len(vm) - 2):  # so it won't calculate anything unexpected
                apd = np.abs(rep_time - act_time)  # calculate the APD

                apd_list.append(apd)
                act_time_list.append(act_time)

                act_start = rep_id
                dvmax = 0.0
                flag_apd = 0

            l += 1

        di_list = []  # diastolic interval list
        for i in range(len(act_time_list) - 1):
            di_list.append((act_time_list[i + 1] - act_time_list[i]) - apd_list[i])
        di_list.append((t[-1] - act_time_list[-1]) - apd_list[-1])

        # print ("\nx: %s" % di_list)
        # print ("\ny: %s" % apd_list)

        pg.plot(x=di_list, y=apd_list, pen=(255, 0, 0), title="APD Restitution")

    def setPlotLineColor(self, color, file):
        """
        Set the plot line color to red, green, blue, yellow or black
        """
        if file == 1:
            mypen1.setColor(QtGui.QColor(color))
            for k in self.central_widget.lista:
                k.plot.clear()
                k.plot.plot(x=k.timeAxis, y=k.variable, pen=mypen1)
                if self.central_widget.data2 is not None:
                    k.plot.plot(x=k.timeAxis, y=k.variable2, pen=mypen2)
        else:
            mypen2.setColor(QtGui.QColor(color))
            for k in self.central_widget.lista:
                k.plot.clear()
                k.plot.plot(x=k.timeAxis, y=k.variable, pen=mypen1)
                k.plot.plot(x=k.timeAxis, y=k.variable2, pen=mypen2)

    def setPlotLineWidth(self, width, file):
        """
        Set the plot line color to red, green, blue, yellow or black
        """
        if file == 1:
            mypen1.setWidth(width)
            for k in self.central_widget.lista:
                k.plot.clear()
                k.plot.plot(x=k.timeAxis, y=k.variable, pen=mypen1)
                if self.central_widget.data2 is not None:
                    k.plot.plot(x=k.timeAxis, y=k.variable2, pen=mypen2)
        else:
            mypen2.setWidth(width)
            for k in self.central_widget.lista:
                k.plot.clear()
                k.plot.plot(x=k.timeAxis, y=k.variable, pen=mypen1)
                k.plot.plot(x=k.timeAxis, y=k.variable2, pen=mypen2)

    def aboutLimpet(self):
        """
        Show limpetGUI about
        """
        QMessageBox.about(self, "About LimpetGUI", """New version of LimpetGUI using PyQtGraph""")


class LimpetGUI(QtWidgets.QWidget):

    def __init__(self, stateVarList, data, groups):
        super(LimpetGUI, self).__init__()

        self.svlist = stateVarList
        self.svcheck = []

        self.data1 = data[0]
        self.data2 = data[1]

        self.group1 = groups[0]
        self.group2 = groups[1]

        self.maxPlot = 12
        self.numPlotPerLine = 4

        # draw user interface
        self.initUI()

    def initUI(self):

        # labels and buttons
        title = QLabel('State Variables')

        replot = QtWidgets.QPushButton("Replot")
        check_all = QtWidgets.QPushButton("Check all")
        uncheck_all = QtWidgets.QPushButton("Uncheck all")
        replot.clicked.connect(self.update_plot)
        check_all.clicked.connect(self.check_all)
        uncheck_all.clicked.connect(self.uncheck_all)

        self.singleZoom = True
        zoom = QtWidgets.QCheckBox('Universal Zoom', self)
        zoom.stateChanged.connect(self.updateZoom)

        # state variables checkbox list
        lst = QtWidgets.QListWidget()
        lst.setMaximumWidth(180)

        for i in range(len(self.svlist)):
            new_string = '%s' % (self.svlist[i])
            new_item = QtWidgets.QListWidgetItem(new_string)
            if i < self.maxPlot:
                new_item.setCheckState(QtCore.Qt.Checked)
                self.svcheck.append(QtCore.Qt.Checked)
            else:
                new_item.setCheckState(QtCore.Qt.Unchecked)
                self.svcheck.append(QtCore.Qt.Unchecked)
            new_item.setTextAlignment(0x0001)  # Qt::AlignCenter
            lst.insertItem(i, new_item)

        self.list_widget = lst
        self.list_widget.itemChanged[QListWidgetItem].connect(self.update_items)

        self.plt = pg.GraphicsLayoutWidget()
        if len(self.svlist) != 0:
            self.plot()

        # configure layout
        grid = QtWidgets.QGridLayout()
        grid.setSpacing(10)

        grid.addWidget(title, 1, 0)
        grid.addWidget(lst, 2, 0)
        grid.addWidget(zoom, 3, 0)
        grid.addWidget(replot, 4, 0)
        grid.addWidget(check_all, 5, 0)
        grid.addWidget(uncheck_all, 6, 0)
        grid.addWidget(self.plt, 2, 1, 7, 1)

        self.setLayout(grid)
        self.show()

    #    @profile
    def plot(self):
        """
        Draw all plots
        """

        plt = self.plt
        data1 = self.data1
        data2 = self.data2

        nrow = ceil(self.maxPlot / self.numPlotPerLine)
        ncol = int(self.numPlotPerLine)

        print("nrow: %s, ncol: %s" % (nrow, ncol))

        plt.clear()

        tname = search_var(data1, 'time', self.group1)
        t = np.array(data1[self.group1][tname])
        # vname = search_var(data1, 'potential', self.group1)
        # vm = np.array(data1[self.group1][vname])

        lr = pg.LinearRegionItem([0, 1000])
        lr.setZValue(-10)
        numPlot = 0
        irow, jcol = 0, 0

        # plot all state variables
        self.lr = lr
        self.lista = []

        for svname, svchk in zip(self.svlist, self.svcheck):

            # if checked then plot
            if svchk == QtCore.Qt.Checked:
                sv = data1[self.group1][svname]
                sv = np.array(sv)
                ss = "%s" % svname

                if data2 is None:
                    svplot = SVPlot(t, sv, None, plt.addPlot(title=ss, row=irow, col=jcol), self.lista)
                else:
                    tname = search_var(data2, 'time', self.group2)
                    t2 = np.array(data2[self.group2][tname])
                    sv2 = data2[self.group2][svname]
                    sv2 = np.array(sv2)
                    ss2 = "%s" % svname
                    svplot = SVPlot(t, sv, sv2, plt.addPlot(title=ss2, row=irow, col=jcol), self.lista, t2)

                self.lista.append(svplot)

                numPlot += 1
                jcol += 1

            if numPlot % self.numPlotPerLine == 0:
                plt.nextRow()
                irow += 1
                jcol = 0
            if numPlot == self.maxPlot: break

    def update_items(self, myint):
        """
        Update the properties (on/off) of the state variables whenever the
        state variable is checked or unchecked in the list_widget
        """
        self.update = True

        state = myint.checkState()
        mtmp = myint.text().split("\t")
        svar = str(mtmp[0])

        for i in range(len(self.svlist)):
            if self.svlist[i] == svar:
                if self.svcheck[i] == QtCore.Qt.Unchecked:
                    self.svcheck[i] = QtCore.Qt.Checked
                    self.maxPlot += 1
                else:
                    self.svcheck[i] = QtCore.Qt.Unchecked
                    self.maxPlot -= 1
                break

        # update the number of plots per line for a prettier visualization
        aux = 2
        while True:
            if self.maxPlot <= aux ** 2:
                self.numPlotPerLine = aux
                break
            else:
                aux += 1

        print(self.maxPlot)

    def update_plot(self):
        """
        This is called when the button replot is pressed
        """
        print("Reploting...")
        self.plot()
        if not self.singleZoom:
            self.updateZoom(QtCore.Qt.Checked)
        else:
            self.updateZoom(QtCore.Qt.Unchecked)

    def check_all(self):
        """
        Called when the button check all is pressed
        """
        for i in range(len(self.svcheck)):
            if self.svcheck[i] == QtCore.Qt.Unchecked:
                self.list_widget.item(i).setCheckState(QtCore.Qt.Checked)

    def uncheck_all(self):
        """
        Called when the button uncheck all is pressed
        """
        for i in range(len(self.svcheck)):
            if self.svcheck[i] == QtCore.Qt.Checked:
                self.list_widget.item(i).setCheckState(QtCore.Qt.Unchecked)

    def updateZoom(self, state):
        """
        Enable zoom to all plots
        """
        if state == QtCore.Qt.Checked:
            self.singleZoom = False
            for i in self.lista:
                i.connectSignal(single=False)
        else:
            self.singleZoom = True
            self.plot()
            for i in self.lista:
                i.connectSignal(single=True)
